# README #

Quick guide to using this tool.

### What is Succulent for? ###

* A set of scripts that can be used to estimate the bitrate of chiplets mounted on a 2.5D Interposer IC
* Estimates bitrate using geometry and well known electrical properties such as resistivity etc.
* All properties and geometries default to a 65nm design featuring two chiplets on a 3mm 32bit bus. 
* Version 0.9.1


### How do I get set up? ###

* Requires the following python packages: numpy, scipy
* Quickstart; import interposersystem as suc; suc.InterposerSystem(); suc.systemBitrate()
* To explore electrical and geometry properties, play with the classes defined in materials.py
* To explore different bus widths and types, play with the classes in bus.py
* For experimenting with different system bus connectivity and spacing, see interposersystem.py

### Contribution guidelines ###

* You can extend the circuits to include loads and drivers by defining transfer functions. See transferfunctions.py and interposersystem.py
* You can use the dimension and offset properties of chiplets if you wish to define an advanced bus length estimation algorithm

### Who do I talk to? ###

* Talk to the hand
* Or, mbarrow@eng.ucsd.edu
