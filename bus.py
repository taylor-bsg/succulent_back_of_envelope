"""@package docstring
Bus Class

"""

import components as c

class BusNode:
  """Describes a component on a bus and its average trace length to the bus master"""
  def __init__( self,
                component = c.ChipletComponent(),
                length = 3e-3 ):
    self.component = component
    self.length = length
    return

class Bus:
  """Describes logical connectivity of components bus and its width and length
  The length(s) must be provided by the user for reasonable estimations and are respective to a
  single bus master. eg average trace length from node A to master node M

  All units are SI, so scientific notation is recommended
  """
  def __init__( self, 
                name="Full_Duplex_32bit_Master_Slave",
                fullDuplex = True,
                w = 32,
                vdd = 1.2,
                nodes = [ BusNode(  component = c.ChipletComponent(name="M"),
                                    length = 0                                ),
                          BusNode(  c.ChipletComponent(name="A",x=12e-3),
                                    length = 3e-3                             ) ],
                ):
    """Parameter description:
       fullDuplex: node to node bitrate will be halved if = false
       w: bus width or number of data lines
       nodes[] an array of connected components"""
       
    self.name = name
    self.fullDuplex = fullDuplex
    self.w = w
    self.vdd = 1.2
    self.nodes = nodes
    return

  def allLengths(self):
    """return the lengths and respective names of all bus nodes
    """
    lengths = [] 
    for node in self.nodes:
      lengths.append((node.length,node.component.name))
    return lengths

  def maxLength(self):
    """return the critical path length for this Bus
    """
    return max(p.length for p in self.nodes)
    

