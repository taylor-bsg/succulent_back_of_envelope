"""@package docstring
Components Classes

Component classes are used as building blocks for a system of chiplets
They define channels of communication in the system, so that the bitrate
of these channels can be estimated. They also specify the type of material
used for these channels using the "Materials" classes
"""

import materials

class InterposerComponent:
  """Describes pysical dimensions of an interposer, along with its material
  properties. By default an early 65nm TSMC Turn key interposer offering is 
  used.

  All units are SI, so scientific notation is recommended
  """
  def __init__( self, 
                name="TSMC_65nm_40x26mm_interposer",
                l=40e-3, 
                w=26e-3, 
                material=materials.InterposerMaterial()):
    self.name = name
    self.l = l
    self.w = w
    self.material = material
    return

class ChipletComponent:
  """Describes physical dimensions of an interposer mounted chiplet, along with
  its material properties. By default a minimum sized tile from an early 65nm 
  TSMC Turn key interposer offering is used.

  All units are SI, so scientific notation is recommended
   """ 
  def __init__( self,
                name="TSMC_65nm_12x13mm_chiplet",
                x=0,
                y=0,
                l=12e-3,
                w=13e-3,
                material=materials.MicroBumpGridMaterial()):
    self.name = name
    self.x = x
    self.y = y
    self.l = l
    self.w = w
    self.material = material
    return

