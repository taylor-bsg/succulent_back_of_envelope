#ballpark bitrate estimates based on the google spreadsheet:
#https://docs.google.com/spreadsheets/d/1I16wYksrZyTVce7OIW-Qnlsg2HImYx8ms9fBw5afniY/edit#gid=0
#can calculate trace parasitics with get_trace_parasitics()
#can estimate trace performance with get_trace_bitrate()i
import math as m
import numpy as np


#helper function to return the 3db of an RLC circuit
def RLC_3db(	R_ohm,
		L_pH,
		C_fF):
	RLC_fr = 1/( 2*m.pi*m.sqrt( (L_pH*m.pow(10,-12))  * (C_fF*m.pow(10,-15))  ) )
	
	RLC_Q = (1/R_ohm)*m.sqrt((L_pH*m.pow(10,-12))/(C_fF*m.pow(10,-15)))

	#calculate the high frequency
	fh = RLC_fr *(m.sqrt(1+(1/4*(RLC_Q*RLC_Q)))+(1/2*RLC_Q))
	return fh

#Determine bitrate and regime of a transmission line.
def get_transmission_line_bitrate(Z_o,
                                  R_ohm,
                                  L_pH,
                                  C_fF,
                                  dbg ):        
	
	#Calculate which regime to be in
	if(R_ohm >= 5*Z_o):
		T_effect = False
	else:
		T_effect = True

	#If you must consider transmission effects
	#should you consider lossless transmission lines?
	if(R_ohm <= (Z_o/2)):
		L_Less_t_line = True
	else:
		L_Less_t_line = False

	#if you should consider transmission effects
	#but should not consider lossless, you must
	#consider lossy lines
	#if(T_effect and L_Less_t_line):
	Lossy_t_line = False
	if(T_effect and not L_Less_t_line):
		Lossy_t_line = True

	#dbg
	if(dbg == True):
		print "Z_o: " + str(Z_o)
		print "T_effect: " + str(T_effect)
		print "L_Less_t_line: " + str(L_Less_t_line)
		print "Lossy_t_line: " + str(Lossy_t_line)

	RC_fh 	  = 1/(2*m.pi*R_ohm*(C_fF*m.pow(10,-15)))
	L_Less_fh = 1/(2*m.pi*(L_pH*m.pow(10,-12))*(C_fF*m.pow(10,-15)))
	Lossy_fh  = RLC_3db(R_ohm,L_pH,C_fF)

	#dbg
	if(dbg == True):
		print "RC fmax: " + str(RC_fh)
		print "Lossless fmax: " + str(L_Less_fh)
		print "Lossy_t_line: " + str(Lossy_fh)

	#if you are an RC line, just return 3db of RC circuit
	if(not T_effect):
		return (RC_fh,"RC_line")
	#if you are lossless, estimate br with 3db of LC circuit
	if(L_Less_t_line):
		return (L_Less_fh,"Lossless_line")
	#if you are lossy, estimate br with 3db of RLC circuit
	if(Lossy_t_line):
		return (Lossy_fh,"Lossy_line")


"""
     <--pitch-->
    ___       ___
   /   \     /   \  /\
  |  Q  |   | 0V  | 2d 
   \___/     \___/  \/
    ___       ___
   /   \     /   \  
  | +V  |   |     |  
   \___/     \___/  
   
   Bottom up view of ubump grid.
   We worry about C and L between Q and 0V
"""
def get_ubump_parasitics( d=9,        #wire RADIUS    um
                          l=10,       #wire length    um
                          pitch=20,   #wire pitch     um
                          Rho=1.71e-8,#wire resistivity. Cu assumed default
                          Er=1.0005): #usually permittivity is that of air
  """
  calculates ubump parasitics based on geometry, conductivity of the ubump
  and the permittivity of the material separating them
  d = RADIUS of the ubump in um (d by convention)
  l = length of the ubump in um
  Rho = resistivity of the ubump
  er = relative permittivity of the dielectric

  by default the ubumps are Cu and are separated by air.
  Formula are well known. These formulations are useful for ballparking
  Inductance and propagation delay.
  """
 
  Z_o = 120/np.sqrt(Er) * np.arccosh(pitch/d)   #Characteristic impedance
  R_Ohm = Rho*l*1e-6 /(((d*1e-6)**2)*np.pi)     #resistance of ubump
  Tpd_ps_um = 3.333e-3 * np.sqrt(Er)            #Tpd for 1um length of ubump
  C_pF = Tpd_ps_um / Z_o * l                    #C of the ubump
  L_nH = Tpd_ps_um * Z_o / 1000 * l             #l of the ubump
  return R_Ohm, L_nH, C_pF, Tpd_ps_um, Z_o

#propegation delay of u_bumps is calculated along with other parasitics
def get_ubump_delay(  d=9,
                      l=10,
                      pitch=20,
                      Rho = 1.71e-8,
                      Er = 1.0005):

  r, l, c, Tpd_ps_um = get_ubump_parasitics(d,
                                            l,
                                            pitch,
                                            Rho,
                                            Er)
  return Tpd_ps_um

"""
  -------     ------ /\
  |<-w->|<-s->|####| t
/\-------     ------ \/
h
\/------------------
  |######GND#######| 

  cross section view of interposer
"""
#All dimensions are in um
def get_trace_parasitics(w=0.4,		#width
			 h=2,		#height
			 t=0.9,		#thickness
			 s=6,		#space
			 l=1000,	#length (um)
			 Rho=1.71e-8,	#resistivity
			 Kr=3.90,	#rel permitivitty
			 Eo=8.85e-3,	#Air dielectric
			 Mc=1,		#Miller Cap
			 dbg=False):	#Debug print

	
	l=l*m.pow(10,-3) #convert to mm, to be in line with the
			 #spreadsheet formula for easier debug
	
	Eox = Kr*Eo
	#propegation velocity along trace
	vp = 299793000/m.sqrt(Kr) #meters/sec
	#Plate capacitance per um (fF/um)
	Cp_fum = Eo*Kr*((1.5*w/h)+2.8*((t/h)**0.222)) 
	#Edge capacitance per um (fF/um)
	Ce_fum = 2*Eo*Kr*(0.03*w/h + 0.83*t/h - 0.07*(t/h)**0.222)*(s/h)**-1.34
	#Resistance in ohms per um
	R_oum = (Rho*0.000001)/((w*m.pow(10,-6))*(t*m.pow(10,-6)))
	#Inductance ballpark per um (fH/um)
	L_fum = Kr/Cp_fum

	#dbg
	if(dbg==True):
		print "Eox: " 	+ str(Eox)
		print "vp: " 	+ str(vp)
		print "L_fum: " + str(L_fum)
		print "Cp_um: " + str(Cp_fum)
		print "Ce_um: " + str(Ce_fum)
		print "R_oum: " + str(R_oum)
	
	#Trace parameters
	R_ohm = (Rho*(l*m.pow(10,-3)))/((w*m.pow(10,-6))*(t*m.pow(10,-6))) 
	L_ph = (L_fum*(l*m.pow(10,3)))*m.pow(10,-3)
	C_fF = (Cp_fum + Ce_fum*Mc)*(l*m.pow(10,3))

	#dbg
	if(dbg==True):
		print "R_ohm: " + str(R_ohm)
		print "L_ph: " + str(L_ph)
		print "C_fF: " + str(C_fF)

	return (R_ohm,L_ph,C_fF)

#ballpark the bitrate
def get_trace_bitrate(w=0.4,
			h=2,
			t=0.9,
			s=6,
			l=1000,
			Rho=1.71e-8,
			Kr=3.90,
			Eo=8.85e-3,
			Mc=1,
			dbg=False   ):
	
	#get the parameters
	(R_ohm,L_pH,C_fF) = get_trace_parasitics(w,
						 h,
						 t,
						 s,
						 l,
						 Rho,
						 Kr,
						 Eo,
						 Mc,
						 dbg  )
        
        #Z_o ballpark estimation
	Z_o = 1/(m.sqrt(L_pH/C_fF))
        
        return get_transmission_line_bitrate( Z_o,
                                              R_ohm,
                                              L_pH,
                                              C_fF,
                                              dbg   )


def get_ubump_bitrate(  d=18,         #wire RADIUS    um
                        l=10,         #wire length    um
                        pitch=20,     #wire pitch     um
                        Rho = 1.71e-8,#wire resistivity. Cu assumed default
                        Er = 1.0005,  #usually permittivity is that of air
                        dbg=False     ):
  """
  Estimate the bitrate of a ubump using transmission line theory.

  l = length / height of ubump in um
  pitch = spacing of ubumps in um
  Rho = resistivity of ubump. Cu is assumed
  er = relative permittivity of medium between ubumps. Air is assumed

  the use of this is mainly to know what regime the ubump is in.
  For example, is it an RC model? is it a Lossless model? Is it lossy?
  The bitrate is not so useful, because the signal goes through several cpts
  """
  #get the parasitics for this component 
  (R_ohm, L_nH, C_pF, Tpd_ps_um, Z_o) = get_ubump_parasitics( d,
                                                              l,
                                                              pitch,
                                                              Rho,
                                                              Er    )
  L_pH = L_nH * 1e3 #TODO: use consistent units. All pH All fF 
  C_fF = C_pF * 1e3
  return get_transmission_line_bitrate( Z_o,
                                        R_ohm,
                                        L_pH,
                                        C_fF,
                                        dbg   )
       
