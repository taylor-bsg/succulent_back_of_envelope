"""@package docstring
Interposer System Class

A class describing an interposer system populated with Chiplets and
connected with a system of busses. These busses can be analyzed to explore
possible system bitrates in the design space
"""

import bus as b
import components as c
import materials as m
import computeparasitics as pc
import transferfunctions as tf

"""class BitrateReport:
  def __init__( self,
                sysbusbr,
                busbr,  #array
"""

def getComponentHs( regime,
                    R_Ohm,
                    L_H,
                    C_F  ):
  """Will create a low pass H(s) = b/a transfer function from parasitics
  depending on what transmission line regime the component could operate in
  
  regime  = (string) regime       e.g. \"RC_line\" or \"Lossless_line\" or \"Lossy_line\"
  R_Ohm   = Resistance in Ohms    e.g. 10e3 for 10K Ohm
  L_H     = Inductance in Henry   e.g. 1e-9 for 1nH
  C_F     = Capacitance in Farrad e.g. 1e-6 for 1uF

  returns: b = [num] a = [den] where H(s) = b/a
  """
  
  if (regime == "RC_line"):
    b, a = tf.get_RC_lp_Hs(R_Ohm,C_F)
    return b, a

  if (regime == "Lossless_line"):
    b, a = tf.get_LC_lp_Hs(L_H,C_F)
    return b, a  
  if (regime == "Lossy_line"):
    b, a = tf.get_RLC_lp_Hs(R_Ohm,L_H,C_F)
    return b, a

  print "Error: Badly defined component transmission line regime"
  return [], []

class InterposerSystem:
  """Describes a connected interposer system of chips and busses.

  The system should be built from the ground up by defining material, components
  and busses
  """
  def __init__( self, 
                name="TSMC_65nm_40x26mm_interposer_design",
                interposer  = c.InterposerComponent(),  
                chiplets    = [ c.ChipletComponent(name="M"),
                                c.ChipletComponent(name="A") ],
                busses      = [b.Bus(nodes= [ b.BusNode(c.ChipletComponent( name="M"),
                                                                            length=0    ),
                                              b.BusNode(c.ChipletComponent( name="A"),
                                                                            length=3e-3 )]) ]
              ):
    """
    Parameters:
    string              name: human readable system name
    InterposerComponent interposer: the interposer component that acts as system substrate
    ChipletComponent[]  chiplets: a list of positioned chiplets that are connected to the substrate
    Bus[]               busses: a list of the bus connectivity for chips in the "chiplet" list
    
    Default constructor creates a 65nm system similar to an early TSMC interposer.
    the system features two adjacent chiplets 'M' and 'A'
    The chiplets are connected by a single bus that is 3mm, 32bits and full duplex"""
    self.name = name
    self.interposer = interposer
    self.chiplets = chiplets
    self.busses = busses
    return

  def systemBitrate(  self,
                      verbose = True,
                      simpleCalc = True):
    """A function to return a list of chiplet names and their bitrates
    The first chiplet is the interposer itself, and a sum of all bus worst case bitrates
    """
    if not simpleCalc:
      print "TODO: Simulation not yet implemented"
      return

    busbr = []
    
    m = self.interposer.material #short ref to the physical properties of the interposer

    #get all bus bitrates
    for bus in self.busses:
      #TODO: factor in ubumps
      #get the critical path of the bus
      critlen = bus.maxLength()
      singlebr,model = pc.get_trace_bitrate(w   = m.w * 1e6, #specified in um,
                                            h   = m.h * 1e6, #convert from SI
                                            s   = m.s * 1e6, #To um
                                            l   = critlen * 1e6,
                                            Rho = m.rho,
                                            Kr  = m.Kr      ) #compute a ball park bitrate
      
      
      
      totalbr = singlebr * bus.w                                    #multiply by number of datalines
      if not bus.fullDuplex:
        totalbr = totalbr / 2                                         #if lines are uni-directional, halve br
      busbr.append((bus.name,totalbr,model))
    
    systembr = sum(br[1] for br in busbr)                         #sum total br 
    if verbose:
      print "Total system interposer trace only bitrate is estimated at %e" %(systembr)
      print "\n-----------------------------------------------------\n"


    if verbose:
      print "%s Bus trace critical path bitrate summary" %self.name
      for sysbus in busbr:
        print "The %s bus bitrate was estimated at %e b/s with a %s model (ubumps NOT accounted for)" %(sysbus[0],sysbus[1],sysbus[2])

      print "\n-----------------------------------------------------\n"
     
    busnodebr = []
    
    #get all node bitrates. This should include ubumps
    for bus in self.busses:
      
      nodebr = []
      ubab = []
      ubaa = []
      
      for node in bus.nodes:
        
        if node.length == 0:
          nodebr.append((node.component.name,0,"BUS_MASTER",0)) #Master has no bus bandwidth to itself
          #get master ubump model
          ua = node.component.material
          bumpabr,bumpamodel = pc.get_ubump_bitrate(  d=ua.r * 1e6,
                                                      l=ua.h * 1e6,
                                                      pitch=ua.p * 1e6,
                                                      Rho = ua.rho,
                                                      Er = ua.Er    )
          #Now get the parasitic components for the bus master ubumps
          ubaR_Ohm, ubaL_nH, ubaC_pF, Tpd, Zo = pc.get_ubump_parasitics( d=ua.r * 1e6,
                                                                  l=ua.h * 1e6,
                                                                  pitch=ua.p * 1e6,
                                                                  Rho = ua.rho,
                                                                  Er = ua.Er      )       
  
          #convert to base 10 Si units
          ubaL_H = ubaL_nH * 1e-9
          ubaC_F = ubaC_pF * 1e-12
          
          #and the transfer function for the bus master ubumps
          (ubab, ubaa) = getComponentHs(  bumpamodel,
                                          ubaR_Ohm,
                                          ubaL_H,
                                          ubaC_F  )         
      #Calculate individual bitrates
      for node in bus.nodes:
        critlen = node.length
        if critlen != 0:
          ub = node.component.material
          #TODO: decide which pitch to take.
          bumpbbr,bumpbmodel = pc.get_ubump_bitrate(  d=ub.r * 1e6,
                                                      l=ub.h * 1e6,
                                                      pitch=ub.p * 1e6,
                                                      Rho = ub.rho,
                                                      Er = ub.Er      )
          
          #get this chiplets trace segment bitrate (distance from itself to the bus master)
          chiplet_tracebr,chiplet_tracemodel = pc.get_trace_bitrate(w = m.w * 1e6,
                                                                    h = m.h * 1e6,
                                                                    s = m.s * 1e6,
                                                                    l = critlen * 1e6,
                                                                    Rho = m.rho,
                                                                    Kr = m.Kr       )

          #Calculate the bitrate proper using transfer functions to combine the appropriate models
          #We are interested in H(s) = [s(ua) * s(m) * s(ub)], or a convolution of all components
          #Frequency responses

          #first get the parasitic components for the trace segment length 
          (tR_Ohm, tL_pH, tC_fF) = pc.get_trace_parasitics( w = m.w * 1e6,
                                                            h = m.h * 1e6,
                                                            s = m.s * 1e6,
                                                            l = critlen * 1e6,
                                                            Rho = m.rho,
                                                            Kr = m.Kr       )


          #convert to base 10 Si units
          tL_H = tL_pH * 1e-12
          tC_F = tC_fF * 1e-15 
          
          #Next express the parasitic components as a H(s)= b/a  transfer function
          #the transfer function is chosen depending on what regime the component could operate in
          (tb, ta) = getComponentHs(  chiplet_tracemodel,
                                      tR_Ohm,
                                      tL_H,
                                      tC_F )

          #Now get the parasitic components for the chiplet ubumps
          #(ubbR_Ohm, ubbL_nH, ubbC_pF) =
          ubbR_Ohm, ubbL_nH, ubbC_pF, Tpd, Zo = pc.get_ubump_parasitics( d=ub.r * 1e6,
                                                                l=ub.h * 1e6,
                                                                pitch=ub.p * 1e6,
                                                                Rho = ub.rho,
                                                                Er = ub.Er      )       
  
          #convert to base 10 Si units
          ubbL_H = ubbL_nH * 1e-9
          ubbC_F = ubbC_pF * 1e-12
          
          #and get the transfer function for the chiplet ubumps
          (ubbb, ubba) = getComponentHs(  bumpbmodel,
                                          ubbR_Ohm,
                                          ubbL_H,
                                          ubbC_F  )
          
          #Combine all H(s)'s by convolving them together to form one polynomial
          b, a = tf.combine_Hs( [ tb,       #numerator of the bus
                                  ubab,     #numerator of the slave chiplet ubump
                                  ubbb  ],  #numerator of the bus master chiplet ubump
                                [ ta,       #denominator of the bus
                                  ubaa,     #denominator of the bus master chiplet ubump
                                  ubba  ] ) #denominator of the slave chiplet ubump

          #Estimate bitrate for the ubump->traceline->ubump system
          chiplet_bump_tracebr = tf.get_Hs_bitrate(b,a)

          totalbr = chiplet_bump_tracebr * bus.w
          if not bus.fullDuplex:
            totalbr = totalbr / 2
          
          #Calculate the switching energy for this connection
          energy = ((ubbC_F + ubaC_F + tC_F) * bus.vdd**2) / 2.0

          nodebr.append((node.component.name,totalbr,model,energy)) #saave all node br's for this bus      
      
    busnodebr.append(nodebr) #accumulate total br for all nodes on all  busses
    
    if verbose:
      print "%s system Chip bitrate summary (ubump<->trace<->ubump estimate)\n" %self.name
      i = 0
      for busnode in busnodebr:
        print "\n-----------------------------------------------------\n"
        print "\tList of Chiplet to bus master bitrates for the %s bus with bus vdd = %f:\n" %(self.busses[i].name,bus.vdd)
        print "\t\tChiplet\tbitrate\testimation model\tswitching energy"
        for chip in busnode:
          print "\t\t%s\t%e b/s\t%s" %(chip[0],chip[1],chip[2])
        i = i+1

    return

