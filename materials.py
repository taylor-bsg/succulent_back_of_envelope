"""@package docstring
Material class definitions.

Material classes are used to pass pysical inromation about
the materials which system components are made from
they define all physical properties required for bitrate estimation
"""

class InterposerMaterial:
  """Describes pysical properties of an interposer
  Dimensions are in SI units, you should use scientific notation for them
  Each parameter has a default value. By default a 65nm SiO2 interposer is created.
  """
  def __init__( self,
                name = "65nm_SiO2_interposer",
                w=0.4e-6, 
                h=2e-6, 
                s=6e-6, 
                l=1e-3,
                rho=1.7e-8,
                Kr=3.9):
    self.name = name
    self.w = w
    self.h = h
    self.s = s
    self.l = l
    self.rho = rho
    self.Kr = Kr
    return

class MicroBumpGridMaterial:
  """Describes physical properties of a microbump grid
  Dimensions are in SI units, you should use scientific notation for them
  Each parameter has a default value. By default a 3.5um bump array is created. Air separation is assumed.
  Default values are taken from "Contact Testing of Copper Micro-pillars with very low damage for 3D IC Assembly"
  O Yaglioglu, 2013 IEEE Intertational Conference on 3D System Integration
   """ 
  def __init__( self,
                name = "20um_Cu_single_pitch_ubump",
                r=9e-6,
                h=10e-6,
                p=20e-6,
                tp=20e-6,
                rho=1.71e-8,
                Er=1.0005):
    """Constructor for chiplets with different longditudinal and transverse
    microbump pitch"""
    self.name = name
    self.r = r
    self.h = h
    self.p = p  #Longditudinal pitch
    self.tp = tp#Transverse pitch
    self.rho = rho
    self.Er = Er
    return

  def __init__( self,
                name = "20um_Cu_two_pitch_ubump",
                r=9e-6,
                h=10e-6,
                p=20e-6,
                rho=1.71e-8,
                Er=1.0005):
    """Constructor for interposers with uniform microbump pitch"""
    self.name = name
    self.r = r
    self.h = h
    self.p = p  #Longditudinal pitch
    self.tp = p #Transverse pitch is identical to p
    self.rho = rho
    self.Er = Er
    return

