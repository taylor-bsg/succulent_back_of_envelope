"""@package docstring
polynomial form transfer functions for considered component connections
These transfer functions can be solved for S to estimate bitrate for all
the component connection topologies we consider

basic filtering theory and notation is used, s.t.

Hs or H(s) = Transfer function / circuit description in polynomial form
b = transfer function numerator
a = transfer function denominator
convolve(a_1,..,a_n) = a means of combining transfer functions 
bode() = a means of evaluating H(s) at frequencies of interest
3db = 1/2 signal amplitude, taken to be cut off for information transfer
"""

from scipy.signal import lti
import numpy as np

#formulation is here:
#http://sim.okawa-denshi.jp/en/CRlowkeisan.htm

def get_RC_lp_Hs(r,c):
  """
  get H(s) = b/a co-effs for a RC lowpass filter
  
  r = resistance in ohms  e.g. 10e3   for 10K
  c = capacitance in F    e.g. 1.5e-6 for 1.5uF
  """

  b = [1/(r*c)]
  a = [1,(1/(r*c))]
  return b, a

#formulation is here:
#http://sim.okawa-denshi.jp/en/RLClowkeisan.htm
def get_RLC_lp_Hs(r,l,c):
  """
  get H(s) = b/a co-effs for a RLC lowpass filter
  
  r = resistance in ohms  e.g. 10e3   for 10K
  l = inductance in H     e.g. 1e-9   for 1nH
  c = capacitance in F    e.g. 1.5e-6 for 1.5uF
  """
  b = [1/(l*c)]
  a = [1,(r/l),1/(l*c)]
  
  return b, a

#usefull for lossless transmission line estimates
#checked against this: http://circuitcalculator.com/lcfilter.htm
def get_LC_lp_Hs(l,c):
  """
  get H(s) = b/a co-effs for a LC lowpass filter
  l = inductance in H     e.g 1e-9 for 1nH
  c = capacitance in F    e.g 1.5e-6 for 1.5uF
  """
  b = [l]
  a = [1,l,1/(l*c)]

  return b, a

def combine_Hs(b_list,a_list):
  """
  get H(s) = H(s) * H'(s) = b/a co-effs by combining H(s)'s with  convolution.
  
  b_list = list of all H(s) numerators    e.g. [[62.9],[30.2]]
  a_list = list of all H(s) denominators  e.g. [[1,62.9],[1,30.2]]
  """ 
  B = b_list[0]
  i = 1
  for b in b_list[1:]:
    B = np.convolve(B,b)
    i = i+1

  A = a_list[0]
  i = 1
  for a in a_list[1:]:
    A = np.convolve(A,a)
    i = i+1
  
  return B, A

def get_Hs_bitrate(b,a):
  """
  Estimate bitrate from H(s) = b/a. For back of the envolope, br = 3db point.
  
  b = polynomial numerator
  a = polynomial denominator
  """
  sl = lti(b,a)
  w, mag, phase = sl.bode()
  i, db = min(enumerate(mag), key=lambda x: abs(x[1]+3)) #the -3db point
  br = w[i]/(2*np.pi) #we define br in bits/sec, not rads/sec
  
  return br


